// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:startup_namer/main.dart';

void main() {
  /*
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());

    // Verify that our counter starts at 0.
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // Verify that our counter has incremented.
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);
  });
  */

  testWidgets('Test from List Page ', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());

    // Find title
    expect(find.text('Startup Name Generator'), findsOneWidget);

    // Find Icon Favorite without border
    expect(find.byIcon(Icons.favorite_border), findsWidgets);

    // No Find Icon Favorite
    expect(find.byIcon(Icons.favorite), findsNothing);

    // Find Icon List
    expect(find.byIcon(Icons.list), findsOneWidget);

    // Tap the '<3' icon and trigger a color change.
    //await tester.tap(find.byIcon(Icons.favorite_border));
    //await tester.pump();

    // Find Icon Favorite
    //expect(find.byIcon(Icons.favorite), findsWidgets);
  });

  testWidgets('Test from Favorite list ', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());

    // Tap List icon and trigger a new page
    await tester.tap(find.byIcon(Icons.list));
    await tester.pump();

    // Find title
    expect(find.text('Saved Suggestions'), findsOneWidget);

  });
}
